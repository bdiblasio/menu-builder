import { useEffect, useState } from "react";

function ListMenuItems() {
  const [menuItems, setMenuItems] = useState([]);
  const getData = async () => {
    const response = await fetch("http://localhost:8000/api/menuitems/");
    if (response.ok) {
      const data = await response.json();
      setMenuItems(data);
    }
  };
  useEffect(() => {
    getData();
  }, []);


  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Name</th>
          <th>Photo</th>
          <th>Description </th>
          <th>Price </th>
          <th>Category </th>
          <th>Flags </th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        {menuItems?.map((menuItem) => {
          return (
            <tr key={menuItem.id}>
              <td>{menuItem.name}</td>
              <td>{menuItem.picture}</td>
              <td>{menuItem.description}</td>
              <td>{menuItem.price}</td>
              <td>{menuItem.category}</td>
              <td>{menuItem.flags}</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

export default ListMenuItems;
