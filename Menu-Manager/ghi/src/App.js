import ListMenuItems from './components/ListMenuItems'

function App() {
  return (
    <div className="App">
      <ListMenuItems/>
    </div>
  );
}

export default App;
