from django.db import models
from model_utils import Choices
from multiselectfield import MultiSelectField

menu_category_choices = (
    ("breakfast", "Breakfast"),
    ("brunch", "Brunch"),
    ("lunch", "Lunch"),
    ("dinner", "Dinner"),
    ("dessert", "Dessert"),
    ("drinks", "Drinks"),
    ("cocktails", "Cocktails"),
    ("beer", "Beer"),
    ("wine", "Wine"),
)

dietary_flag_choices = (
    ("vegan", "Vegan"),
    ("vegetarian", "Vegetarian"),
    ("pescatarian", "Pescatarian"),
    ("halal", "Halal"),
    ("kosher", "Kosher"),
    ("soy_free", "No Soy"),
    ("dairy_free", "DF"),
    ("gluten_free", "GF"),
    ("treenut_free", "No Tree Nuts"),
    ("peanut_free", "No Peanuts"),
    ("all_nut_free", "No Nuts"),
    ("shellfish_free", "No Shellfish")
)

# If blank=True then the field is not required
# If null=True then the DB will set NULL on the column

class MenuItem(models.Model):
    name=models.CharField(max_length=100)
    picture = models.ImageField(blank=True, null=True, upload_to='static')
    description = models.TextField(blank=True, null=True)
    price = models.DecimalField(
        max_digits=5, decimal_places=2, blank=True, null=True)
    category= MultiSelectField(choices=menu_category_choices, max_choices=5, max_length=100, blank=True, null=True)
    flags= MultiSelectField(choices=dietary_flag_choices, max_choices=10, max_length=100, blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "Menu Items"
