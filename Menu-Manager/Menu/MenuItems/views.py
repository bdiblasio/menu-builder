from django.shortcuts import render
from rest_framework import viewsets
from .serializers import MenuItemSerializer
from .models import MenuItem

# Create your views here.
def Home(request):
    return render(request, "home.html")

class MenuItemView(viewsets.ModelViewSet):
    serializer_class = MenuItemSerializer
    queryset = MenuItem.objects.all()
