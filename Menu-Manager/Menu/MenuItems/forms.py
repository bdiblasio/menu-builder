from models import MenuItem
from django.forms import ModelForm

class MenuItemForm(ModelForm):
    class Meta:
        model=MenuItem
        fields=['myfields']
